use bevy::{
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    prelude::*,
    time::common_conditions::on_timer,
};
use rand::{rngs::SmallRng, Rng as _, SeedableRng};
use std::time::Duration;

const SUBSTEPS: usize = 3;
const DT: f32 = 1.0 / 144.0;
const SUB_DT: f32 = DT / SUBSTEPS as f32;
const INV_SUB_DT: f32 = 1.0 / SUB_DT;
const GRAVITY: Vec2 = Vec2::new(0.0, -2.81);
const MARBLE_RADIUS: f32 = 0.1;
const BOX_SIZE: Vec2 = Vec2::new(0.3, 0.2);
const SAFETY_MARGIN_FACTOR: f32 = 2.0 * DT;

#[derive(Component)]
pub struct PrevPos(pub Vec2);

#[derive(Component)]
pub struct Vel(pub Vec2);

#[derive(Component)]
pub struct PreSolveVel(pub Vec2);

#[derive(Component)]
pub struct CircleCollider {
    pub radius: f32,
}

#[derive(Component)]
pub struct BoxCollider {
    size: Vec2,
}

#[derive(Resource)]
pub struct CollisionPairs(pub Vec<(Entity, Entity)>);

#[derive(Resource)]
pub struct Contacts(pub Vec<(Entity, Entity, Vec2)>);

#[derive(Resource)]
pub struct StaticContacts(pub Vec<(Entity, Vec2)>);

#[derive(Component)]
pub struct Aabb {
    min: Vec2,
    max: Vec2,
}

impl Aabb {
    pub fn intersects(&self, other: &Self) -> bool {
        self.max.x >= other.min.x
            && self.max.y >= other.min.y
            && self.min.x <= other.max.x
            && self.min.y <= other.max.y
    }

    pub fn from_pos_half_box_size(pos: Vec2, half_box_size: Vec2) -> Self {
        Self {
            min: pos - half_box_size,
            max: pos + half_box_size,
        }
    }
}

#[derive(Bundle)]
pub struct DynBundle<C>
where
    C: Component + Send + Sync + 'static,
{
    prev_pos: PrevPos,
    vel: Vel,
    pre_solve_vel: PreSolveVel,
    collider: C,
    aabb: Aabb,
}

#[derive(Bundle)]
pub struct StaticBundle<C>
where
    C: Component + Send + Sync + 'static,
{
    collider: C,
}

#[derive(Resource)]
pub struct Meshes {
    pub quad: Handle<Mesh>,
    pub sphere: Handle<Mesh>,
}

#[derive(Resource)]
pub struct Materials {
    pub white: Handle<StandardMaterial>,
}

#[derive(Resource)]
pub struct Rng(pub SmallRng);

fn box_box_contact(
    transform_a: &Transform,
    transform_b: &Transform,
    box_a: &BoxCollider,
    box_b: &BoxCollider,
) -> Option<(f32, Vec2)> {
    let half_a = 0.5 * box_a.size;
    let half_b = 0.5 * box_b.size;

    let ab = transform_b.translation.xy() - transform_a.translation.xy();
    let overlap = (half_a + half_b) - ab.abs();

    if overlap.x < 0.0 || overlap.y < 0.0 {
        return None;
    }

    if overlap.x < overlap.y {
        let depth = overlap.x;
        let n = Vec2::new(ab.x.signum(), 0.0);
        Some((depth, n))
    } else {
        let depth = overlap.y;
        let n = Vec2::new(0.0, ab.y.signum());
        Some((depth, n))
    }
}

fn box_circle_contact(
    transform_box: &Transform,
    transform_circle: &Transform,
    r#box: &BoxCollider,
    circle: &CircleCollider,
) -> Option<(f32, Vec2)> {
    let box_to_circle = transform_circle.translation.xy() - transform_box.translation.xy();
    let box_to_circle_abs = box_to_circle.abs();
    let half_size = 0.5 * r#box.size;
    if box_to_circle_abs.x > half_size.x + circle.radius
        || box_to_circle_abs.y > half_size.y + circle.radius
    {
        return None;
    }

    let corner_to_center = box_to_circle_abs - half_size;
    let s = box_to_circle.xy().signum();

    if corner_to_center.x > corner_to_center.y {
        // Closer to vertical edge.
        Some((circle.radius - corner_to_center.x, Vec2::new(s.x, 0.0)))
    } else {
        // Closer to horizontal edge.
        Some((circle.radius - corner_to_center.y, Vec2::new(0.0, s.y)))
    }
}

fn spawn_camera(mut commands: Commands) {
    commands.spawn(Camera3dBundle {
        transform: Transform::from_translation(Vec3::new(0.0, 0.0, 100.0)),
        projection: Projection::Orthographic(OrthographicProjection {
            scale: 0.01,
            ..Default::default()
        }),
        ..Default::default()
    });
}

fn spawn_meshes(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let quad = meshes.add(Mesh::from(shape::Quad::new(BOX_SIZE)));

    let sphere = meshes.add(
        Mesh::try_from(shape::Icosphere {
            radius: MARBLE_RADIUS,
            subdivisions: 4,
        })
        .unwrap(),
    );
    commands.insert_resource(Meshes { quad, sphere });

    let white = materials.add(StandardMaterial {
        base_color: Color::WHITE,
        unlit: true,
        ..Default::default()
    });

    commands.insert_resource(Materials {
        white: white.clone(),
    });

    let size = Vec2::new(16.0, 1.5);
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(size))),
            material: white,
            transform: Transform::from_xyz(0.0, -3.0, 0.0),
            ..Default::default()
        },
        StaticBundle {
            collider: BoxCollider { size },
        },
    ));
}

fn spawn_box_or_marble(
    mut commands: Commands,
    meshes: Res<Meshes>,
    materials: Res<Materials>,
    mut rng: ResMut<Rng>,
) {
    let pos = Vec2::new(
        rng.0.gen_range(-0.40..=0.40),
        3.0 + rng.0.gen_range(-0.40..=0.40),
    );
    let vel = Vel(Vec2::new(
        rng.0.gen::<f32>() - 0.5,
        rng.0.gen::<f32>() - 0.5,
    ));
    let prev_pos = PrevPos(pos - vel.0 * DT);
    let pre_solve_vel = PreSolveVel(Vec2::ZERO);
    let transform = Transform::from_translation(pos.extend(0.0));

    if rng.0.gen::<bool>() {
        let half_box_size = Vec2::splat(MARBLE_RADIUS);

        commands.spawn((
            PbrBundle {
                mesh: meshes.sphere.clone(),
                material: materials.white.clone(),
                transform,
                ..Default::default()
            },
            DynBundle {
                prev_pos,
                vel,
                pre_solve_vel,
                collider: CircleCollider {
                    radius: MARBLE_RADIUS,
                },
                aabb: Aabb::from_pos_half_box_size(pos, half_box_size),
            },
        ));
    } else {
        let half_box_size = 0.5 * BOX_SIZE;

        commands.spawn((
            PbrBundle {
                mesh: meshes.quad.clone(),
                material: materials.white.clone(),
                transform,
                ..Default::default()
            },
            DynBundle {
                prev_pos,
                vel,
                pre_solve_vel,
                collider: BoxCollider { size: BOX_SIZE },
                aabb: Aabb::from_pos_half_box_size(pos, half_box_size),
            },
        ));
    }
}

fn despawn_boxes(mut commands: Commands, q: Query<(Entity, &Transform)>) {
    for (entity, transform) in &q {
        if transform.translation.y < -5.0 {
            if let Some(mut entity) = commands.get_entity(entity) {
                entity.despawn();
            }
        }
    }
}

fn update_aabb_circle(mut q: Query<(&mut Aabb, &Transform, &Vel, &CircleCollider)>) {
    for (mut aabb, transform, v, circle) in &mut q {
        let safety_margin = SAFETY_MARGIN_FACTOR * v.0.length();
        let half_box = Vec2::splat(circle.radius + safety_margin);
        let pos = transform.translation.xy();
        aabb.min = pos - half_box;
        aabb.max = pos + half_box;
    }
}

fn update_aabb_box(mut q: Query<(&mut Aabb, &Transform, &Vel, &BoxCollider)>) {
    for (mut aabb, transform, v, r#box) in &mut q {
        let safety_margin = SAFETY_MARGIN_FACTOR * v.0.length();
        let half_box = 0.5 * r#box.size + safety_margin;
        let pos = transform.translation.xy();
        aabb.min = pos - half_box;
        aabb.max = pos + half_box;
    }
}

fn collect_collision_pairs(q: Query<(Entity, &Aabb)>, mut collision_pairs: ResMut<CollisionPairs>) {
    collision_pairs.0.clear();

    let mut iter = q.iter_combinations();
    while let Some([(entity_a, aabb_a), (entity_b, aabb_b)]) = iter.fetch_next() {
        if aabb_a.intersects(aabb_b) {
            collision_pairs.0.push((entity_a, entity_b));
        }
    }
}

fn integrate(mut q: Query<(&mut Transform, &mut PrevPos, &mut PreSolveVel, &mut Vel)>) {
    for (mut transform, mut prev_pos, mut pre_solve_v, mut v) in &mut q {
        prev_pos.0 = transform.translation.xy();
        v.0 += SUB_DT * GRAVITY;
        transform.translation.x += SUB_DT * v.0.x;
        transform.translation.y += SUB_DT * v.0.y;
        pre_solve_v.0 = v.0;
    }
}

fn dyn_circle_dyn_circle_solve_pos(
    mut q: Query<(&mut Transform, &CircleCollider)>,
    mut contacts: ResMut<Contacts>,
    collision_pairs: Res<CollisionPairs>,
) {
    for (entity_a, entity_b) in &collision_pairs.0 {
        let Ok([(mut transform_a, circle_a), (mut transform_b, circle_b)]) =
            q.get_many_mut([*entity_a, *entity_b])
        else {
            continue;
        };

        let ab = transform_b.translation.xy() - transform_a.translation.xy();
        let ab_len_sq = ab.length_squared();
        let radius_sum = circle_a.radius + circle_b.radius;

        if ab_len_sq < radius_sum * radius_sum {
            let ab_len = ab_len_sq.sqrt();
            let depth = radius_sum - ab_len;
            let n = ab / ab_len;
            let shift = (0.5 * depth) * n;

            transform_a.translation.x -= shift.x;
            transform_a.translation.y -= shift.y;
            transform_b.translation.x += shift.x;
            transform_b.translation.y += shift.y;

            contacts.0.push((*entity_a, *entity_b, n));
        }
    }
}

fn dyn_box_dyn_box_solve_pos(
    mut q: Query<(&mut Transform, &BoxCollider)>,
    mut contacts: ResMut<Contacts>,
    collision_pairs: Res<CollisionPairs>,
) {
    for (entity_a, entity_b) in &collision_pairs.0 {
        let Ok([(mut transform_a, box_a), (mut transform_b, box_b)]) =
            q.get_many_mut([*entity_a, *entity_b])
        else {
            continue;
        };

        let Some((depth, n)) = box_box_contact(&transform_a, &transform_b, box_a, box_b) else {
            continue;
        };

        let shift = depth * n;
        transform_a.translation.x -= shift.x;
        transform_a.translation.y -= shift.y;
        transform_b.translation.x += shift.x;
        transform_b.translation.y += shift.y;

        contacts.0.push((*entity_a, *entity_b, n))
    }
}

fn dyn_circle_dyn_box_solve_pos(
    mut q_box: Query<(&mut Transform, &BoxCollider)>,
    mut q_circle: Query<(&mut Transform, &CircleCollider), Without<BoxCollider>>,
    mut contacts: ResMut<Contacts>,
    collision_pairs: Res<CollisionPairs>,
) {
    'outer: for (entity_a, entity_b) in &collision_pairs.0 {
        let ((mut transform_box, r#box), (mut transform_circle, circle)) = 'inner: {
            if let (Ok(b), Ok(c)) = (q_box.get_mut(*entity_a), q_circle.get_mut(*entity_b)) {
                break 'inner (b, c);
            }
            if let (Ok(b), Ok(c)) = (q_box.get_mut(*entity_b), q_circle.get_mut(*entity_a)) {
                break 'inner (b, c);
            }
            continue 'outer;
        };

        let Some((depth, n)) = box_circle_contact(&transform_box, &transform_circle, r#box, circle)
        else {
            continue;
        };

        let shift = depth * n;
        transform_box.translation.x -= shift.x;
        transform_box.translation.y -= shift.y;
        transform_circle.translation.x += shift.x;
        transform_circle.translation.y += shift.y;

        contacts.0.push((*entity_a, *entity_b, n))
    }
}

fn static_box_dyn_circle_solve_pos(
    mut q_dynamics: Query<(Entity, &mut Transform, &CircleCollider), With<PrevPos>>,
    q_statics: Query<(&Transform, &BoxCollider), Without<PrevPos>>,
    mut contacts: ResMut<StaticContacts>,
) {
    for (entity_circle, mut transform_circle, circle) in &mut q_dynamics {
        for (transform_box, r#box) in &q_statics {
            let Some((depth, n)) =
                box_circle_contact(transform_box, &transform_circle, r#box, circle)
            else {
                continue;
            };

            let shift = depth * n;
            transform_circle.translation += shift.extend(0.0);
            contacts.0.push((entity_circle, n));
        }
    }
}

fn static_box_dyn_box_solve_pos(
    mut q_dynamics: Query<(Entity, &mut Transform, &BoxCollider), With<PrevPos>>,
    q_statics: Query<(&Transform, &BoxCollider), Without<PrevPos>>,
    mut contacts: ResMut<StaticContacts>,
) {
    for (entity_a, mut transform_a, box_a) in &mut q_dynamics {
        for (transform_b, box_b) in &q_statics {
            let Some((depth, n)) = box_box_contact(&transform_a, transform_b, box_a, box_b) else {
                continue;
            };

            let shift = depth * n;
            transform_a.translation.x -= shift.x;
            transform_a.translation.y -= shift.y;

            contacts.0.push((entity_a, n))
        }
    }
}

fn update_vel(mut q: Query<(&Transform, &PrevPos, &mut Vel)>) {
    for (transform, prev_pos, mut v) in &mut q {
        v.0 = (transform.translation.xy() - prev_pos.0) * INV_SUB_DT;
    }
}

fn solve_vel(mut q: Query<(&mut Vel, &PreSolveVel)>, mut contacts: ResMut<Contacts>) {
    for (entity_a, entity_b, n) in contacts.0.drain(..) {
        let [(mut v_a, pre_solve_v_a), (mut v_b, pre_solve_v_b)] =
            q.get_many_mut([entity_a, entity_b]).unwrap();

        let pre_solve_relative_v = pre_solve_v_a.0 - pre_solve_v_b.0;
        let pre_solve_normal_v = pre_solve_relative_v.dot(n);

        let relative_v = v_a.0 - v_b.0;
        let normal_v = relative_v.dot(n);

        let shift = (0.5 * (normal_v + pre_solve_normal_v)) * n;

        v_a.0 -= shift;
        v_b.0 += shift;
    }
}

fn static_solve_vel(mut q: Query<(&mut Vel, &PreSolveVel)>, mut contacts: ResMut<StaticContacts>) {
    for (entity, n) in contacts.0.drain(..) {
        let (mut v, pre_solve_v) = q.get_mut(entity).unwrap();

        let pre_solve_normal_v = pre_solve_v.0.dot(n);
        let normal_v = v.0.dot(n);

        let shift = (0.5 * (normal_v + pre_solve_normal_v)) * n;

        v.0 -= shift;
    }
}

fn main() {
    let mut window = Window::default();
    window.set_maximized(true);

    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(Time::<Fixed>::from_duration(Duration::from_secs_f32(
            SUB_DT,
        )))
        .insert_resource(CollisionPairs(Vec::with_capacity(1024)))
        .insert_resource(Contacts(Vec::with_capacity(1024)))
        .insert_resource(StaticContacts(Vec::with_capacity(1024)))
        .insert_resource(Rng(SmallRng::seed_from_u64(42)))
        .add_plugins((
            DefaultPlugins.set(WindowPlugin {
                primary_window: Some(window),
                ..Default::default()
            }),
            LogDiagnosticsPlugin::default(),
            FrameTimeDiagnosticsPlugin,
        ))
        .add_systems(Startup, (spawn_camera, spawn_meshes))
        .add_systems(
            FixedUpdate,
            (
                integrate,
                dyn_circle_dyn_circle_solve_pos,
                dyn_box_dyn_box_solve_pos,
                dyn_circle_dyn_box_solve_pos,
                static_box_dyn_circle_solve_pos,
                static_box_dyn_box_solve_pos,
                update_vel,
                solve_vel,
                static_solve_vel,
            )
                .chain(),
        )
        .add_systems(
            Update,
            (
                (
                    (update_aabb_box, update_aabb_circle),
                    collect_collision_pairs,
                )
                    .chain()
                    .run_if(on_timer(Duration::from_secs_f32(DT))),
                (spawn_box_or_marble, despawn_boxes).run_if(on_timer(Duration::from_millis(300))),
            ),
        )
        .run();
}
