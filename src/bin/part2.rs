use bevy::{prelude::*, time::common_conditions::on_timer};
use rand::{rngs::SmallRng, Rng as _, SeedableRng};
use std::time::Duration;

const INV_DT: f32 = 144.0;
const DT: f32 = 1.0 / INV_DT;
const MARBLE_RADIUS: f32 = 0.1;
const GRAVITY: Vec3 = Vec3::new(0.0, -2.81, 0.0);

#[derive(Component)]
pub struct PrevPos(pub Vec3);

#[derive(Component)]
pub struct Vel(pub Vec3);

#[derive(Component)]
pub struct PreSolveVel(pub Vec3);

#[derive(Component)]
pub struct CircleCollider {
    pub radius: f32,
}

#[derive(Component)]
pub struct BoxCollider {
    size: Vec2,
}

#[derive(Resource)]
pub struct Contacts(pub Vec<(Entity, Entity, Vec3)>);

#[derive(Resource)]
pub struct StaticContacts(pub Vec<(Entity, Vec3)>);

#[derive(Bundle)]
pub struct ParticleBundle {
    prev_pos: PrevPos,
    vel: Vel,
    pre_solve_vel: PreSolveVel,
    collider: CircleCollider,
}

#[derive(Bundle)]
pub struct StaticBoxBundle {
    collider: BoxCollider,
}

#[derive(Resource)]
pub struct Meshes {
    pub sphere: Handle<Mesh>,
}

#[derive(Resource)]
pub struct Rng(pub SmallRng);

#[derive(Resource)]
pub struct Materials {
    pub white: Handle<StandardMaterial>,
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let sphere = meshes.add(
        Mesh::try_from(shape::Icosphere {
            radius: MARBLE_RADIUS,
            subdivisions: 4,
        })
        .unwrap(),
    );

    commands.insert_resource(Meshes { sphere });

    let white = materials.add(StandardMaterial {
        base_color: Color::WHITE,
        unlit: true,
        ..Default::default()
    });

    commands.insert_resource(Materials {
        white: white.clone(),
    });

    let size = Vec2::new(7.0, 1.5);
    commands.spawn((
        PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::new(size))),
            material: white,
            transform: Transform::from_xyz(0.0, -3.0, 0.0),
            ..Default::default()
        },
        StaticBoxBundle {
            collider: BoxCollider { size },
        },
    ));

    commands.spawn(Camera3dBundle {
        transform: Transform::from_translation(Vec3::new(0.0, 0.0, 100.0)),
        projection: Projection::Orthographic(OrthographicProjection {
            scale: 0.01,
            ..Default::default()
        }),
        ..Default::default()
    });
}

fn spawn_marble(
    mut commands: Commands,
    meshes: Res<Meshes>,
    materials: Res<Materials>,
    mut rng: ResMut<Rng>,
) {
    let pos = Vec3::new(
        rng.0.gen_range(-0.25..=0.25),
        3.0 + rng.0.gen_range(-0.25..=0.25),
        0.0,
    );
    let vel = Vec3::new(rng.0.gen::<f32>() - 0.5, rng.0.gen::<f32>() - 0.5, 0.0);

    commands.spawn((
        PbrBundle {
            mesh: meshes.sphere.clone(),
            material: materials.white.clone(),
            transform: Transform::from_translation(pos),
            ..Default::default()
        },
        ParticleBundle {
            prev_pos: PrevPos(pos - vel * DT),
            vel: Vel(vel),
            pre_solve_vel: PreSolveVel(Vec3::ZERO),
            collider: CircleCollider {
                radius: MARBLE_RADIUS,
            },
        },
    ));
}

fn despawn_marbles(mut commands: Commands, q: Query<(Entity, &Transform)>) {
    for (entity, transform) in &q {
        if transform.translation.y < -5.0 {
            if let Some(mut entity) = commands.get_entity(entity) {
                entity.despawn();
            }
        }
    }
}

fn integrate(mut q: Query<(&mut Transform, &mut PrevPos, &mut PreSolveVel, &mut Vel)>) {
    for (mut transform, mut prev_pos, mut pre_solve_v, mut v) in &mut q {
        prev_pos.0 = transform.translation;
        v.0 += DT * GRAVITY;
        transform.translation += DT * v.0;
        pre_solve_v.0 = v.0;
    }
}

fn solve_pos(
    mut q: Query<(Entity, &mut Transform, &CircleCollider)>,
    mut contacts: ResMut<Contacts>,
) {
    let mut iter = q.iter_combinations_mut();
    while let Some([(entity_a, mut transform_a, circle_a), (entity_b, mut transform_b, circle_b)]) =
        iter.fetch_next()
    {
        let ab = transform_b.translation - transform_a.translation;
        let ab_length_sq = ab.length_squared();
        let radius_sum = circle_a.radius + circle_b.radius;

        if ab_length_sq < radius_sum * radius_sum {
            let ab_length = ab_length_sq.sqrt();
            let depth = radius_sum - ab_length;
            let n = ab / ab_length;
            let shift = (0.5 * depth) * n;

            transform_a.translation -= shift;
            transform_b.translation += shift;

            contacts.0.push((entity_a, entity_b, n));
        }
    }
}

fn solve_pos_static(
    mut q_dynamics: Query<(Entity, &mut Transform, &CircleCollider)>,
    q_statics: Query<(&Transform, &BoxCollider), Without<CircleCollider>>,
    mut contacts: ResMut<StaticContacts>,
) {
    for (circle_entity, mut circle_transform, circle_collider) in &mut q_dynamics {
        for (box_transform, box_collider) in &q_statics {
            let box_to_circle = circle_transform.translation - box_transform.translation;
            let box_to_circle_abs = box_to_circle.abs();
            let half_size = 0.5 * box_collider.size;
            if box_to_circle_abs.x > half_size.x + circle_collider.radius
                || box_to_circle_abs.y > half_size.y + circle_collider.radius
            {
                continue;
            }

            let corner_to_center = box_to_circle_abs.xy() - half_size;
            let s = box_to_circle.xy().signum();

            let (n, shift) = if corner_to_center.x > corner_to_center.y {
                // Closer to vertical edge.
                (
                    Vec2::new(s.x, 0.0),
                    Vec2::new(s.x * (circle_collider.radius - corner_to_center.x), 0.0),
                )
            } else {
                // Closer to horizontal edge.
                (
                    Vec2::new(0.0, s.y),
                    Vec2::new(0.0, s.y * (circle_collider.radius - corner_to_center.y)),
                )
            };

            circle_transform.translation += shift.extend(0.0);
            contacts.0.push((circle_entity, n.extend(0.0)));
        }
    }
}

fn update_vel(mut q: Query<(&Transform, &PrevPos, &mut Vel)>) {
    for (transform, prev_pos, mut v) in &mut q {
        v.0 = (transform.translation - prev_pos.0) * INV_DT;
    }
}

fn solve_vel(mut q: Query<(&mut Vel, &PreSolveVel)>, mut contacts: ResMut<Contacts>) {
    for (entity_a, entity_b, n) in contacts.0.drain(..) {
        let [(mut v_a, pre_solve_v_a), (mut v_b, pre_solve_v_b)] =
            q.get_many_mut([entity_a, entity_b]).unwrap();

        let pre_solve_relative_v = pre_solve_v_a.0 - pre_solve_v_b.0;
        let pre_solve_normal_v = pre_solve_relative_v.dot(n);

        let relative_v = v_a.0 - v_b.0;
        let normal_v = relative_v.dot(n);

        let shift = (0.5 * (normal_v + pre_solve_normal_v)) * n;

        v_a.0 -= shift;
        v_b.0 += shift;
    }
}

fn solve_vel_static(mut q: Query<(&mut Vel, &PreSolveVel)>, mut contacts: ResMut<StaticContacts>) {
    for (entity, n) in contacts.0.drain(..) {
        let (mut v, pre_solve_v) = q.get_mut(entity).unwrap();

        let pre_solve_normal_v = pre_solve_v.0.dot(n);
        let normal_v = v.0.dot(n);

        let shift = (0.5 * (normal_v + pre_solve_normal_v)) * n;

        v.0 -= shift;
    }
}

fn main() {
    let mut window = Window::default();
    window.set_maximized(true);

    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(Time::<Fixed>::from_duration(Duration::from_secs_f32(DT)))
        .insert_resource(Contacts(Vec::with_capacity(64)))
        .insert_resource(StaticContacts(Vec::with_capacity(64)))
        .insert_resource(Rng(SmallRng::seed_from_u64(42)))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(window),
            ..Default::default()
        }))
        .add_systems(Startup, setup)
        .add_systems(
            FixedUpdate,
            (
                integrate,
                solve_pos,
                solve_pos_static,
                update_vel,
                solve_vel,
                solve_vel_static,
            )
                .chain(),
        )
        .add_systems(
            Update,
            (
                spawn_marble.run_if(on_timer(Duration::from_millis(200))),
                despawn_marbles,
            ),
        )
        .run();
}
