use bevy::prelude::*;
use std::time::Duration;

const INV_DT: f32 = 144.0;
const DT: f32 = 1.0 / INV_DT;
const GRAVITY: Vec3 = Vec3::new(0.0, -2.81, 0.0);

#[derive(Component)]
pub struct PrevPos(pub Vec3);

#[derive(Component)]
pub struct Vel(pub Vec3);

#[derive(Component)]
pub struct PreSolveVel(pub Vec3);

#[derive(Component)]
pub struct CircleCollider {
    pub radius: f32,
}

#[derive(Resource)]
pub struct Contacts(pub Vec<(Entity, Entity)>);

#[derive(Bundle)]
pub struct ParticleBundle {
    prev_pos: PrevPos,
    vel: Vel,
    pre_solve_vel: PreSolveVel,
    collider: CircleCollider,
}

fn spawn_particle(
    pos: Vec3,
    vel: Vec3,
    radius: f32,
    commands: &mut Commands,
    mesh: Handle<Mesh>,
    material: Handle<StandardMaterial>,
) {
    commands.spawn((
        PbrBundle {
            mesh,
            material,
            transform: Transform::from_translation(pos),
            ..Default::default()
        },
        ParticleBundle {
            prev_pos: PrevPos(pos - vel * DT),
            vel: Vel(vel),
            pre_solve_vel: PreSolveVel(Vec3::ZERO),
            collider: CircleCollider { radius },
        },
    ));
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let radius = 0.5;
    let sphere = meshes.add(
        Mesh::try_from(shape::Icosphere {
            radius,
            subdivisions: 4,
        })
        .unwrap(),
    );

    let white = materials.add(StandardMaterial {
        base_color: Color::WHITE,
        unlit: true,
        ..Default::default()
    });

    spawn_particle(
        Vec3::new(-2.0, 3.0, 0.0),
        Vec3::new(2.0, 0.0, 0.0),
        radius,
        &mut commands,
        sphere.clone(),
        white.clone(),
    );
    spawn_particle(
        Vec3::new(2.0, 3.0, 0.0),
        Vec3::new(-2.0, 0.0, 0.0),
        radius,
        &mut commands,
        sphere,
        white,
    );

    commands.spawn(Camera3dBundle {
        transform: Transform::from_translation(Vec3::new(0.0, 0.0, 100.0)),
        projection: Projection::Orthographic(OrthographicProjection {
            scale: 0.01,
            ..Default::default()
        }),
        ..Default::default()
    });
}

fn integrate(mut q: Query<(&mut Transform, &mut PrevPos, &mut PreSolveVel, &mut Vel)>) {
    for (mut transform, mut prev_pos, mut pre_solve_v, mut v) in &mut q {
        prev_pos.0 = transform.translation;
        v.0 += DT * GRAVITY;
        transform.translation += DT * v.0;
        pre_solve_v.0 = v.0;
    }
}

fn solve_pos(
    mut q: Query<(Entity, &mut Transform, &CircleCollider)>,
    mut contacts: ResMut<Contacts>,
) {
    let mut iter = q.iter_combinations_mut();
    while let Some([(entity_a, mut transform_a, circle_a), (entity_b, mut transform_b, circle_b)]) =
        iter.fetch_next()
    {
        let ab = transform_b.translation - transform_a.translation;
        let ab_length_sq = ab.length_squared();
        let radius_sum = circle_a.radius + circle_b.radius;

        if ab_length_sq < radius_sum * radius_sum {
            let ab_length = ab_length_sq.sqrt();
            let depth = radius_sum - ab_length;
            let n = ab / ab_length;
            let shift = (0.5 * depth) * n;

            transform_a.translation -= shift;
            transform_b.translation += shift;

            contacts.0.push((entity_a, entity_b));
        }
    }
}

fn update_vel(mut q: Query<(&Transform, &PrevPos, &mut Vel)>) {
    for (transform, prev_pos, mut v) in &mut q {
        v.0 = (transform.translation - prev_pos.0) * INV_DT;
    }
}

fn solve_vel(mut q: Query<(&mut Vel, &PreSolveVel, &Transform)>, mut contacts: ResMut<Contacts>) {
    for entities in contacts.0.drain(..) {
        let [(mut v_a, pre_solve_v_a, transform_a), (mut v_b, pre_solve_v_b, transform_b)] =
            q.get_many_mut(entities.into()).unwrap();

        let n = (transform_b.translation - transform_a.translation).normalize();
        let pre_solve_relative_v = pre_solve_v_a.0 - pre_solve_v_b.0;
        let pre_solve_normal_v = pre_solve_relative_v.dot(n);

        let relative_v = v_a.0 - v_b.0;
        let normal_v = relative_v.dot(n);

        let shift = (0.5 * (normal_v + pre_solve_normal_v)) * n;

        v_a.0 -= shift;
        v_b.0 += shift;
    }
}

fn main() {
    let mut window = Window::default();
    window.set_maximized(true);

    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(Time::<Fixed>::from_duration(Duration::from_secs_f32(DT)))
        .insert_resource(Contacts(Vec::with_capacity(64)))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(window),
            ..Default::default()
        }))
        .add_systems(Startup, setup)
        .add_systems(
            FixedUpdate,
            (integrate, solve_pos, update_vel, solve_vel).chain(),
        )
        .run();
}
